const authorize = require('../middlewares/authMiddleware');
const loadController = require('../controllers/loadController');
const { checkShipper, checkDriver } = require('../middlewares/checkRole');
const checkId = require('../middlewares/idValidationMiddleware');
const express = require('express');

const router = express.Router();

router.get('/loads', authorize, loadController.getUserLoads);
router.post('/loads', authorize, checkShipper, loadController.createLoad);
router.get('/loads/active', authorize, checkDriver, loadController.getActiveLoad);
router.patch('/loads/active/state', authorize, checkDriver, loadController.iterateToNextLoadState);
router.get('/loads/:id', checkId, authorize, loadController.getUserLoadById);
router.put('/loads/:id', checkId, authorize, checkShipper, loadController.updateLoadById);
router.delete('/loads/:id', checkId, authorize, checkShipper, loadController.deleteLoadById);
router.post('/loads/:id/post', checkId, authorize, checkShipper, loadController.postLoad);
router.get('/loads/:id/shipping_info', checkId, authorize, checkShipper, loadController.getShippingInfo);

module.exports = router;