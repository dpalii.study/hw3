const express = require('express');

const authController = require('../controllers/userController');
const authorize = require('../middlewares/authMiddleware');
const isOnLoad = require('../middlewares/isOnLoad');

const router = express.Router();

router.get('/users/me', authorize, authController.getMe);
router.delete('/users/me', authorize, isOnLoad, authController.deleteMe);
router.patch('/users/me/password', authorize, isOnLoad, authController.changePassword);

module.exports = router;