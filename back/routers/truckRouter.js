const express = require('express');

const truckController = require('../controllers/truckController');
const authorize = require('../middlewares/authMiddleware');
const { checkDriver } = require('../middlewares/checkRole');
const isOnLoad = require('../middlewares/isOnLoad');

const router = express.Router();

router.get('/trucks', authorize, checkDriver, truckController.getDriverTrucks);
router.post('/trucks', authorize, checkDriver, truckController.addTruckForDriver);
router.get('/trucks/:id', authorize, checkDriver, truckController.getTruckById);
router.put('/trucks/:id', authorize, checkDriver, isOnLoad, truckController.updateTruckById);
router.delete('/trucks/:id', authorize, checkDriver, isOnLoad, truckController.deleteTruckById);
router.post('/trucks/:id/assign', authorize, checkDriver, isOnLoad, truckController.assignTruckById);

module.exports = router;