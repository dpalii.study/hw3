const Credentials = require('../models/credentials');

module.exports = {
    checkDriver: async function (req, res, next) {
        if (req.credentials.role === 'DRIVER') {
            next();
        }
        else {
            res.status(401).json({message: "Only drivers can access this resource"});
        }
    },
    checkShipper: async function (req, res, next) {
        if (req.credentials.role === 'SHIPPER') {
            next();
        }
        else {
            res.status(401).json({message: "Only shippers can access this resource"});
        }
    }
}