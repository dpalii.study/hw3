const Truck = require('../models/truck');

module.exports = async function isOnLoad(req, res, next) {
    const user = req.user;

    try {
        const truckOnLoad = await Truck.findOne({
            assigned_to: user._id,
            status: 'OL'
        }).exec();

        if (truckOnLoad) {
            res.status(400).json({
                message: "Can't change profile or truck info while on load"
            });
        }
        else {
            next();
        }
    }
    catch (e) {
        console.log(e);
        res.status(500).json({message: "Server error"});
    }
}