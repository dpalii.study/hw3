const jwt = require('jsonwebtoken');
const config = require('config');
const Credentials = require('../models/credentials');
const User = require('../models/user');

module.exports = async function authorize(req, res, next) {
    const header = req.headers.authorization;

    if (!header) {
        res.status(401).json({ message: "No authorization header provided" });
        return;
    }

    const [, token] = header.split(' ');
    const secret = process.env.SECRET || (config.has('secret') && config.get('secret')) || 'secret';

    try {
        const jwtCreds = jwt.verify(token, secret);
        const dbCreds = await Credentials.findOne(jwtCreds);

        if (!dbCreds) 
            throw "Credentials in JWT not found in DB";
        
        req.credentials = dbCreds;
        req.user = await User.findOne({email: dbCreds.email});
        next();
    }
    catch(err) {
        res.status(401).json({ message: "JWT is invalid"});
    }
}