module.exports = function handleJoiError(e, res) {
    if (e.isJoi) {
        const message = e.details.map(x => x.message).join('\n');
        res.status(400).json({message: message});
    }
    else {
        console.log(e);
        res.status(500).json({message: "Server error"});
    }
}