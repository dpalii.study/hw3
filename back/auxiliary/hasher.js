const crypto = require('crypto');
const config = require('config');

module.exports = function(str) {
    const salt = process.env.SECRET || (config.has('secret') && config.get('secret')) || 'secret';

    const hash = crypto.createHmac('sha256', salt);
    hash.update(str);
    return hash.digest('hex');
}