const User = require('../models/user');
const Credentials = require('../models/credentials');
const getHash = require('../auxiliary/hasher');

const joi = require('joi');

const updPasswdSchema = joi.object({
    oldPassword: joi.string()
        .required(),
    newPassword: joi.string()
    .required()
});

class UserController {
    static async getMe(req, res) {
        res.status(200).json({user: req.user});
    }

    static async deleteMe(req, res) {
        const user = req.user;

        try {
            const [deletedUser, deletedCredentials] = await Promise.all([
                User.findByIdAndDelete(user._id).exec(),
                Credentials.findOneAndDelete({ email: user.email }).exec()
            ]);

            if (!deletedUser || !deletedCredentials) {
                res.status(400).json({ message: "Profile not found" });
            }
            else {
                res.status(200).json({ message: "Profile deleted successfully" });
            }
        }
        catch(err) {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }

    static async changePassword(req, res) {
        const user = req.user;

        try {
            const { oldPassword, newPassword } = joi.attempt(req.body, updPasswdSchema);

            const credentials = await Credentials.findOneAndUpdate({ 
                email: user.email, 
                password: getHash(oldPassword) 
            }, 
            { 
                password: getHash(newPassword) 
            }).exec();

            if (!credentials) {
                res.status(400).json({ message: "Old password is incorrect" });
                return;
            }
            res.status(200).json({ message: "Password changed successfully" });
        }
        catch(err) {
            if (err.isJoi) {
                const message = e.details.map(x => x.message).join('\n');
                res.status(400).json({message: message});
            }
            else {
                console.log(err);
                res.status(500).json({ message: "Server error" });
            }
        }
    }
}

module.exports = UserController;