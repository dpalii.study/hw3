const Truck = require('../models/truck');
const handleJoiError = require('../auxiliary/handleJoiError');

const joi = require('joi');
const truckProps = require('../auxiliary/truckProps');
const ObjectId = require('mongoose').Types.ObjectId;

const truckSchema = joi.object({
    type: joi.string()
        .valid(
            'SPRINTER',
            'SMALL STRAIGHT',
            'LARGE STRAIGHT'
        )
        .required()
});

class TruckController {
    static async getDriverTrucks(req, res) {
        const user = req.user;

        try {
            const trucks = await Truck.find({created_by: user._id}).exec();

            res.status(200).json({trucks: trucks});
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }
    static async addTruckForDriver(req, res) {
        const user = req.user;

        try {
            const { type } = joi.attempt(req.body, truckSchema);

            const newTruck = new Truck({
                created_by: user._id,
                assigned_to: null,
                type: type,
                status: 'OS',
                created_date: new Date(),
                dimensions: truckProps[type]
            });

            await newTruck.save();

            res.status(200).json({message: "Truck created successfully"});
        }
        catch (e) {
            handleJoiError(e, res);
        }
    }
    static async getTruckById(req, res) {
        const user = req.user;
        const truckId = req.params.id;

        if (!ObjectId.isValid(truckId)) {
            res.status(400).json({message: "Invalid resource ID"});
            return;
        }

        try {
            const truck = await Truck.findOne({created_by: user._id, _id: truckId}).exec();

            if (!truck) {
                res.status(400).json({message: "Truck not found"});
                return;
            }
            res.status(200).json({truck: truck});
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }
    static async updateTruckById(req, res) {
        const user = req.user;
        const truckId = req.params.id;

        if (!ObjectId.isValid(truckId)) {
            res.status(400).json({message: "Invalid resource ID"});
            return;
        }

        try {
            const { type } = joi.attempt(req.body, truckSchema);
            const truck = await Truck.findOneAndUpdate({
                created_by: user._id, 
                _id: truckId
            }, {
                type: type,
                dimensions: truckProps[type]
            }).exec();

            if (!truck) {
                res.status(400).json({message: "Truck not found"});
                return;
            }
            res.status(200).json({message: "Truck details changed successfully"});
        }
        catch (e) {
            handleJoiError(e, res);
        }
    }
    static async deleteTruckById(req, res) {
        const user = req.user;
        const truckId = req.params.id;

        if (!ObjectId.isValid(truckId)) {
            res.status(400).json({message: "Invalid resource ID"});
            return;
        }

        try {
            const truck = await Truck.findOneAndDelete({created_by: user._id, _id: truckId}).exec();

            if (!truck) {
                res.status(400).json({message: "Truck not found"});
                return;
            }

            res.status(200).json({message: "Truck deleted successfully"});
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }
    static async assignTruckById(req, res) {
        const user = req.user;
        const truckId = req.params.id;

        if (!ObjectId.isValid(truckId)) {
            res.status(400).json({message: "Invalid resource ID"});
            return;
        }

        try {
            const oldTruck = await Truck.findOneAndUpdate({
                assigned_to: user._id
            }, {
                status: 'OS',
                assigned_to: null
            }).exec();

            const newTruck = await Truck.findOneAndUpdate({
                created_by: user._id, 
                _id: truckId
            }, {
                status: 'IS',
                assigned_to: user._id
            }).exec(); 

            if (!newTruck) {
                res.status(400).json({message: "Truck not found"});
                return;
            }
            res.status(200).json({message: "Truck assigned successfully"});
        }
        catch (e) {
            handleJoiError(e, res);
        }
    }
}

module.exports = TruckController;