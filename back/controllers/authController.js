const Credentials = require('../models/credentials');
const User = require('../models/user');
const getHash = require('../auxiliary/hasher');

const jwt = require('jsonwebtoken');
const joi = require('joi');
const nodemailer = require('nodemailer');
const config = require('config');

const registerSchema = joi.object({
    email: joi.string()
        .email()
        .required(),
    password: joi.string()
        .required(),
    role: joi.string()
        .valid('SHIPPER', 'DRIVER')
        .required()
});

const loginSchema = joi.object({
    email: joi.string()
        .email()
        .required(),
    password: joi.string()
        .required()
});

const forgotPassSchema = joi.object({
    email: joi.string()
        .email()
        .required()
});

class AuthController {
    static async register(req, res) {

        try {
            const {email, password, role} = joi.attempt(req.body, registerSchema);

            const passHash = getHash(password);

            const existingUser = await Credentials.findOne({email: email}).exec();

            if (existingUser) {
                res.status(400).json({message: "This email is occupied"});
                return;
            }

            const credentials = new Credentials({
                email: email,
                password: passHash,
                role: role
            });
            const user = new User({
                email: email,
                created_date: new Date()
            });

            try {
                await Promise.all([
                    await credentials.save(),
                    await user.save()
                ]);
                res.status(200).json({
                    message: 'Profile created successfully'
                });
            }
            catch (err) {
                console.log(err);
                res.status(500).json({ message: "Server error" });
            }
        }
        catch (e) {
            const message = e.details.map(x => x.message).join('\n');
            res.status(400).json({message: message});
        }
        
    }

    static async login(req, res) {
        try {
            const {email, password} = joi.attempt(req.body, loginSchema);

            const passHash = getHash(password);
    
            try {
                const data = await Credentials.findOne({
                    email: email,
                    password: passHash
                }).exec();
    
                if (data) {
                    const secret = process.env.SECRET || (config.has('secret') && config.get('secret')) || 'secret';
                    const token = jwt.sign(JSON.stringify(data), secret);
    
                    res.status(200).json({ jwt_token: token });
                }
                else {
                    res.status(400).json({ message: "Email or password is incorrect" });
                }
            }
            catch(err) {
                console.log(err);
                res.status(500).json({ message: "Server error" });
            }

        }
        catch (e) {
            const message = e.details.map(x => x.message).join('\n');
            res.status(400).json({message: message});
        }
    }
    
    static async forgotPassword(req, res) {
        try {
            const { email } = joi.attempt(req.body, forgotPassSchema);

            const passwdLength = 10;
            const randomPassword = Math.random().toString(36).substr(2, passwdLength + 2);

            const updatedCredentials = await Credentials.findOneAndUpdate(
                {
                    email: email
                },{
                    password: getHash(randomPassword)
                }).exec();

            if (!updatedCredentials) {
                res.status(400).json({message: "User with such email not found"});
                return;
            }

            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: 'hw3.mailbox@gmail.com', // should be in .env
                    pass: 'qwer1qwer' 
                },
            });
            
            const info = await transporter.sendMail({
                from: 'hw3.mailbox@gmail.com', 
                to: email,
                subject: "Password reset",
                text: `New password: ${randomPassword}`
            });

            res.status(200).json({message: "New password sent to your email address"});
        }
        catch (err) {
            if (e.isJoi) {
                const message = e.details.map(x => x.message).join('\n');
                res.status(400).json({message: message});
            }
            else {
                console.log(err)
                res.status(500).json({message: "Server error"});
            }
        }
    }
}

module.exports = AuthController;