const Load = require('../models/load');
const Truck = require('../models/truck');
const handleJoiError = require('../auxiliary/handleJoiError');

const joi = require('joi');
const ObjectId = require('mongoose').Types.ObjectId;

const paginationQuerySchema = joi.object({
    status: joi.string()
        .default('')
        .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
    limit: joi.number()
        .default(10)
        .integer()
        .min(1)
        .max(50),
    offset: joi.number()
        .default(0)
        .integer()
});

const loadSchema = joi.object({
    name: joi.string()
        .required(),
    payload: joi.number()
        .integer()
        .min(0)
        .required(),
    pickup_address: joi.string()
        .required(),
    delivery_address: joi.string()
        .required(),
    dimensions: joi.object({
        width: joi.number()
            .min(0)
            .required(),
        height: joi.number()
            .min(0)
            .required(),
        length: joi.number()
            .min(0)
            .required(),
    }).required()
});

const loadStates = [
    'Not assigned',
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery'
];

class LoadController {
    static async getUserLoads(req, res) {
        const user = req.user;

        try {
            const { status, limit, offset } = joi.attempt(req.query, paginationQuerySchema);

            const [ loads, count ] = await Promise.all([
                Load.find({
                    $or: [
                        { created_by: user._id },
                        { assigned_to: user._id }
                    ],
                    status: status ?
                        status :
                        { $exists: true }
                })
                    .skip(offset)
                    .limit(limit)
                    .exec(),
                Load.countDocuments({
                    $or: [
                        { created_by: user._id },
                        { assigned_to: user._id }
                    ],
                    status: status ?
                        status :
                        { $exists: true }
                }).exec()
            ]);

            res.status(200).json({loads: loads, count: count});
        }
        catch (e) {
            handleJoiError(e, res);
        }
    }

    static async createLoad(req, res) {
        const user = req.user;

        try {
            const reqLoad = joi.attempt(req.body, loadSchema);

            const newLoad = new Load({
                ...reqLoad,
                logs: [],
                created_date: new Date(),
                created_by: user._id,
                assigned_to: null,
                status: 'NEW',
                state: 'Not assigned'
            });

            await newLoad.save();

            res.status(200).json({message: "Load created successfully"});
        }  
        catch (e) {
            handleJoiError(e, res);
        }
    }

    static async getActiveLoad(req, res) {
        const user = req.user;

        try {
            const activeLoad = await Load.findOne({
                assigned_to: user._id
            }).exec();

            res.status(200).json({
                load: activeLoad ? 
                activeLoad : 
                null
            });
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }

    static async iterateToNextLoadState(req, res) {
        const user = req.user;
        const lastIndex = loadStates.length - 1

        try {
            const activeLoad = await Load.findOne({
                assigned_to: user._id
            }).exec();

            if (!activeLoad) {
                res.status(400).json({message: "Active load not found"});
                return;
            }

            const stateIndex = loadStates.indexOf(activeLoad.state);

            if (stateIndex >= lastIndex) {
                res.status(400).json({
                    message: "Shipment has already been marked as delivered"
                });
                return;
            }

            await Load.findOneAndUpdate({
                assigned_to: user._id
            }, {
                state: loadStates[stateIndex + 1],
                status: stateIndex + 1 === lastIndex ?
                'SHIPPED' : 
                'ASSIGNED'
            }).exec();

            if (stateIndex + 1 === lastIndex) {
                await Truck.findOneAndUpdate({
                    assigned_to: user._id
                }, {
                    status: 'IS'
                });
            }

            res.status(200).json({message: `Load state changed to ${loadStates[stateIndex + 1]}`});
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }
    
    static async getUserLoadById(req, res) {
        const user = req.user;
        const loadId = req.params.id;

        try {
            const load = await Load.findOne({
                    _id: loadId,
                    $or: [
                        { created_by: user._id },
                        { assigned_to: user._id }
                    ]
                }).exec();

            res.status(200).json({load: load});
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }

    static async updateLoadById(req, res) {
        const user = req.user;
        const loadId = req.params.id;

        try {
            const update = joi.attempt(req.body, loadSchema);
            
            const updatedLoad = await Load.findOneAndUpdate({
                status: 'NEW',
                created_by: user._id,
                _id: loadId
            }, update).exec();

            if (!updatedLoad) {
                res.status(400).json({message: `Can't update load with id ${loadId}`});
            }
            else {
                res.status(200).json({message: "Load details changed successfully"});
            }
        }
        catch (e) {
            handleJoiError(e, res);
        }
    }

    static async deleteLoadById(req, res) {
        const user = req.user;
        const loadId = req.params.id;

        try {
            const deletedLoad = await Load.findOneAndDelete({
                status: 'NEW',
                created_by: user._id,
                _id: loadId
            }).exec();

            if (!deletedLoad) {
                res.status(400).json({message: `Can't delete load with id ${loadId}`});
            }
            else {
                res.status(200).json({message: "Load deleted successfully"});
            }
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }

    static async postLoad(req, res) {
        const user = req.user;
        const loadId = req.params.id;
        
        try {
            const postedLoad = await Load.findOneAndUpdate({
                _id: loadId,
                created_by: user._id,
                status: 'NEW'
            }, {
                status: 'POSTED'
            }).exec();

            if (!postedLoad) {
                res.status(400).json({message: `Can't post load with id ${loadId}`});
                return;
            }

            const truck = await Truck.findOneAndUpdate({
                status: 'IS',                
                'dimensions.payload': { $gte: postedLoad.payload },
                'dimensions.width': { $gte: postedLoad.dimensions.width },
                'dimensions.height': { $gte: postedLoad.dimensions.height },
                'dimensions.length': { $gte: postedLoad.dimensions.length }
            }, {
                status: 'OL'
            }).exec();

            if (!truck) {
                await Load.findByIdAndUpdate(loadId, {
                    status: 'NEW',
                    $push: {
                        logs: {
                            message: "Driver not found",
                            time: new Date()
                        }
                    }
                }).exec();

                res.status(200).json({
                    message: "Driver not found",
                    driver_found: false
                });
                return;
            }

            await Load.findByIdAndUpdate(loadId, {
                status: 'ASSIGNED',
                state: 'En route to Pick Up',
                assigned_to: truck.assigned_to,
                $push: {
                    logs: {
                        message: `Load assigned to driver with id ${truck.assigned_to}`,
                        time: new Date()
                    }
                }
            }).exec();

            res.status(200).json({
                message: `Load assigned to driver with id ${truck.assigned_to}`,
                driver_found: true
            });
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }

    static async getShippingInfo(req, res) {
        const user = req.user;
        const loadId = req.params.id;

        try {
            const load = await Load.findOne({
                created_by: user._id,
                _id: loadId
            }).exec();

            if (!load || !load.assigned_to) {
                res.status(400).json({message: "Shipping not found"});
                return;
            }

            const truck = await Truck.findOne({
                assigned_to: load.assigned_to
            }).exec();

            if (!truck) {
                res.status(400).json({message: "Shipping not found"});
                return;
            }

            res.status(200).json({
                load: load,
                truck: truck
            });
        }
        catch (e) {
            console.log(e);
            res.status(500).json({message: "Server error"});
        }
    }
}

module.exports = LoadController;
