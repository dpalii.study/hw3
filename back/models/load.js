const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;
const schema = new mongoose.Schema({
    created_by: {
        type: ObjectId,
        ref: 'user',
        required: true,
        unique: false
    },
    assigned_to: {
        type: ObjectId,
        ref: 'user',
        required: false,
        unique: false
    },
    status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        required: true,
        unique: false
    },
    state: {
        type: String,
        enum: [
            'En route to Pick Up', 
            'Arrived to Pick Up', 
            'En route to delivery',
            'Arrived to delivery',
            'Not assigned'
        ],
        required: true,
        unique: false
    },
    name: {
        type: String,
        required: true,
        unique: false
    },
    payload: {
        type: Number,
        required: true,
        unique: false
    },
    pickup_address: {
        type: String,
        required: true,
        unique: false
    },
    delivery_address: {
        type: String,
        required: true,
        unique: false
    },
    dimensions: {
        width: {
            type: Number,
            required: true,
            unique: false
        },
        length: {
            type: Number,
            required: true,
            unique: false
        },
        height: {
            type: Number,
            required: true,
            unique: false
        }
    },
    logs: [
        {
            message: {
                type: String,
                required: true,
                unique: false
            },
            time: {
                type: Date,
                required: true,
                unique: false
            },
        }
    ],
    created_date: {
        type: Date,
        required: true,
        unique: false
    }
});

module.exports = mongoose.model('load', schema);
