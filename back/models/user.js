const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    created_date: {
        type: Date,
        required: true,
        unique: false
    }
})

module.exports = mongoose.model('user', schema);