const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        unique: false,
        required: true
    },
    role: {
        type: String,
        enum: ['SHIPPER', 'DRIVER'],
        required: true,
        unique: false
    }
});

module.exports = mongoose.model('credentials', schema);