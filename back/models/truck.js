const mongoose = require('mongoose');

const ObjectId = mongoose.Types.ObjectId;
const schema = new mongoose.Schema({
    created_by: {
        type: ObjectId,
        ref: 'user',
        required: true,
        unique: false
    },
    assigned_to: {
        type: ObjectId,
        ref: 'user',
        required: false,
        unique: false
    },
    type: {
        type: String,
        enum: [
            'SPRINTER',
            'SMALL STRAIGHT',
            'LARGE STRAIGHT'
        ],
        required: true,
        unique: false
    },
    dimensions: {
        width: {
            type: Number,
            required: true,
            unique: false
        },
        length: {
            type: Number,
            required: true,
            unique: false
        },
        height: {
            type: Number,
            required: true,
            unique: false
        },
        payload: {
            type: Number,
            required: true,
            unique: false
        }
    },
    status: {
        type: String,
        enum: [
            'OL',
            'IS',
            'OS'
        ],
        required: true,
        unique: false
    },
    created_date: {
        type: Date,
        required: true,
        unique: false
    }
});

module.exports = mongoose.model('truck', schema);