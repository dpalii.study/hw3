**PROJECT DETAILS**

Hometask #3 for EPAM FE lab: UBER-like service for freight trucks, in REST style, using MongoDB as database

**REQUIRED SOFTWARE**

Node.js - [installation link](https://nodejs.org/uk/)

**SETUP AND RUN**

Run this commands in terminal:

1. npm install
2. npm start